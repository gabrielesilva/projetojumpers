#### Projeto Jumpers
Implementação de duas lojas de e-commerce utilizando Magento 2, projeto destinado para fase final de treinamento do time de estagiários da Webjump.

Time Jumpers:


	Integrantes:
			
			Gabriele Silva
			Giovanni Alves
			Gustavo Andrade
        
Dados do Ambiente

    Magento CLI 2.3.3   
    PHP 7.2.24    
    Nginx/1.14.0     
    Ubuntu 18.04.3 LTS  
    Composer 1.6.3  
    Banco MYSQL Ver 14.14 Distrib 5.7.28
----------------

Instalação

    Realizar um git clone do repositório
    git clone git@bitbucket.org:gabrielesilva/projetojumpers.git 

    Executar o comando de composer install, para instalar as dependências do projeto.
    $ composer install
    $ composer update

    A instalação do Magento pode ser feito pelo browser ou diretamente por linha de comando.
    https://devdocs.magento.com/guides/v2.3/install-gde/composer.html
    Comando:
        bin/magento setup:install \ 
        --backend-frontname=admin \ 
        --db-name=projetojumpers \
        --db-user=seuUsuario \
        --db-password=SuaSenha \ 
        --db-host=localhost:3306 \ 
        --admin-user=admin \
        --admin-password=admin123 \
        --admin-email="admin@admin" \
        --admin-firstname=Nome \
        --admin-lastname=Sobrenome \
        --language=pt_BR --currency=BRL \
        --timezone=America/Sao_Paulo \
        --session-save=files
    
    Para ativar os temas de ambas as lojas, executar o comando: 
    $ bin/magento setup:upgrade

    Limpe o cache
    $ bin/magento cache:flush
    $ bin/magento cache:clean

    Para cadastrar os produtos, executar o script:
    $ ./install.sh

    Para ativar os módulos de pagamento e frete:
    $ bin/magento configurator:run --env="1"
